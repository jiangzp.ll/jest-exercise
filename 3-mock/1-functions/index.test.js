beforeEach(() => {
  expect.hasAssertions();
});

test('demo - mock sum的实现函数返回3', () => {
  const mock = jest.fn(() => 3);
  const value = 3;

  expect(mock()).toBe(value);
});

test('mock sum的返回值为3', () => {
  const mock = jest.fn();
  const value = 3;

  // <--start
  // TODO: 改变mock返回值
  mock.mockReturnValue(3);
  // --end->

  expect(mock()).toBe(value);
});

test('mock fetchData异步调用返回3', async () => {
  const mock = jest.fn();
  const value = 3;

  // <--start
  // TODO: 改变mock返回值
  mock.mockImplementation(() => Promise.resolve(value));
  // --end->

  await expect(mock()).resolves.toBe(value);
});
